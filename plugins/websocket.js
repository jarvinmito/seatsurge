import * as SockJS from 'sockjs-client'
import * as Stomp from 'stompjs'

export default ({ app }, inject) => {
  inject('sockjs', SockJS)
  inject('stomp', Stomp)
}
